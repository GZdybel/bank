public interface AlarmListener{

    public void alarmTurnedOn(EnterdPinEvent event);
    public void alarmTurnedOff(EnterdPinEvent event);

}
