import java.util.ArrayList;
import java.util.EventListener;
import java.util.List;

/**
 * Created by Gosia on 21/10/2015.
 */
public class Alarm {

    private String pin;
    private List<AlarmListener> listeners;
    private EnterdPinEvent event;

    private Thread thread = new Thread(){
        public void run(){
            while (true){
                int random = (int)(Math.random()*2);
                event = new EnterdPinEvent();
                switch (random){
                        case 0: correctPin(event);break;
                        case 1: wrongPin(event);break;
                    }
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    };

    public static void main(String args[]){
        Alarm alarm = new Alarm("1234");
        SoundAlert soundAlert = new SoundAlert();
        Bars bars = new Bars();
        Dogs dogs = new Dogs();
        Police police = new Police();
        alarm.addListener(soundAlert);
        alarm.addListener(bars);
        alarm.addListener(dogs);
        alarm.addListener(police);

    }

    private void wrongPin(EnterdPinEvent event) {
        for(AlarmListener a: listeners){
            a.alarmTurnedOn(event);
        }
    }

    private void correctPin(EnterdPinEvent event) {
        for(AlarmListener a: listeners){
            a.alarmTurnedOff(event);
        }
    }

    public Alarm(String pin) {
        this.pin = pin;
        this.listeners = new ArrayList<AlarmListener>();
        thread.start();
    }

    public synchronized void addListener(AlarmListener listener){
        listeners.add(listener);

    }
    public synchronized void removeListener(AlarmListener listener){
        listeners.remove(listener);
    }
    public String getPin(){
        return pin;
    }

}
